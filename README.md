# Clockify Helper

This is basically a hacker tool, so you can fill in some data, and this project converts that 
into time-registrations automatically. 
Just because no-one has time to do it manually.

![Aint nobody](https://miro.medium.com/max/659/1*8xraf6eyaXh-myNXOXkqLA.jpeg)

## Make it work

Copy the environment file:

`cp .env.example .env`

Generate the key for Laravel:

`php artisan key:generate`

Install the composer requirements:

`composer install`

## Environment variables

```dotenv
CLOCKIFY_API_URL=https://api.clockify.me/api/
CLOCKIFY_API_KEY={Your api key}
CLOCKIFY_DEFAULT_PROJECT="{The project name}"
CLOCKIFY_START_WORK_DAY_TIME="08:00"
```

After changing, please clear caching and config:

`php artisan ca:cl && php artisan co:ca`

## Run the sync

`php artisan clockify:sync`