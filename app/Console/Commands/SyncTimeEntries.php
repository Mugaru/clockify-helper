<?php

namespace App\Console\Commands;

use Carbon\Carbon;
use GuzzleHttp\Client;
use Illuminate\Console\Command;
use Illuminate\Support\Collection;

class SyncTimeEntries extends Command
{
    protected $signature = 'clockify:sync';

    protected $description = 'Synchronise time from Clockify';
    private $client;
    private $workSpace;

    public function __construct()
    {
        parent::__construct();

        $this->client = new Client([
            'base_uri' => config('services.clockify.api_url'),
            'headers' => [
                'X-Api-Key' => config('auth.clockify.api_key'),
                'Content-Type' => 'application/json',
                'Accept' => 'application/json',
            ]
        ]);
    }

    public function handle()
    {
        $this->setupWorkspace();

        $project = $this->getProject();
        $user = $this->getUser();

        $timeEntries = $this->getTimeEntries($user);
        $timeEntriesByDate = $timeEntries->groupBy(function($item, $key) {
            return substr($item->timeInterval->start, 0, 10);
        });

        $datesNeeded = $this->getDatesNeeded();
        $start_hour = (int)substr(config('services.clockify.start_work_day_time'), 0, 2);
        $start_minutes = (int)substr(config('services.clockify.start_work_day_time'), 3, 2);

        foreach ($datesNeeded as $dateNeeded) {
            if ($timeEntriesByDate->has($dateNeeded)) continue;

            // Make a new one for this one.
            $start = Carbon::createFromFormat('Y-m-d', $dateNeeded)->setTime($start_hour, $start_minutes);

            $this->client->post(sprintf('v1/workspaces/%s/time-entries', $this->workSpace->id), [
                'json' => [
                    'start' => $start->toIso8601ZuluString(),
                    'end' => $start->copy()->addHours(8)->toIso8601ZuluString(),
                    'billable' => true,
                    'projectId' => $project->id,
                ]
            ]);
        }

        return 0;
    }

    private function setupWorkspace()
    {
        $workSpaces = collect($this->get('v1/workspaces'));
        if (count($workSpaces) === 0) {
            return false;
        }

        $this->workSpace = $workSpaces->first();
    }

    private function get(string $string)
    {
        return \json_decode($this->client->get($string)->getBody()->getContents());
    }

    private function getDatesNeeded(): array
    {
        $dates = [];
        $date = Carbon::now()->subMonth()->startOfDay();
        while ($date < today()) {
            if ($date->isWeekend()) {
                $date->addDay();
                continue;
            }

            $dates[] = $date->format('Y-m-d');
            $date->addDay();
        }

        return $dates;
    }

    /**
     * @return mixed
     */
    private function getProject()
    {
        $project = collect($this->get(sprintf('v1/workspaces/%s/projects', $this->workSpace->id)))
            ->filter(function ($project) {
                return $project->name == config('services.clockify.default_project');
            })
            ->first();

        return $project;
    }

    /**
     * @return mixed
     */
    private function getUser()
    {
        return $this->get('v1/user');
    }

    private function getTimeEntries($user): Collection
    {
        $timeEntries = json_decode($this->client->get(
            sprintf('v1/workspaces/%s/user/%s/time-entries', $this->workSpace->id, $user->id),
            [
                'query' => [
                    'start' => Carbon::now()->subMonth()->startOfDay()->toIso8601ZuluString()
                ]
            ]
        )->getBody()->getContents());

        return collect($timeEntries);
}
}
